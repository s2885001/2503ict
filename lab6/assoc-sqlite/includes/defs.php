<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "pms.php";
include "db_defs.php"; 

/* Search sample data for form data $query in name, year or state. */
/*function search($query) {

 try
{
  $db = db_open();
  $sql = "SELECT pms.* FROM pms WHERE pms.number LIKE '%$query%' OR pms.name LIKE '%$query%' OR pms.start LIKE '%$query%' 
        OR pms.finish LIKE '%$query%' OR pms.duration LIKE '%$query%' OR pms.party LIKE '%$query%' OR pms.state LIKE '%$query%'";
  $result = $db->query($sql);
  // Grab all of the rows as an array
  $pms = $result->fetchAll();
}
*/ 
  
function search($query) {
  $query = trim($query);
  $dbh = db_open();
  
  try {
  $statement = $dbh->prepare("SELECT number, name, start, finish, party, duration, state FROM pms WHERE 
                              name LIKE :query OR start LIKE :query OR finish LIKE :query OR party LIKE :query OR
                              duration LIKE :query OR state LIKE :query");
    
  $statement->bindValue(':query', '%'.$query.'%');
  $statement->execute();
  $results = $statement->fetchAll();
  
  return $results;
  } catch (Exception $e) {
    show_error($e);
  }
 

// Display array in smarty
//$smarty = new Smarty;
//$smarty->assign("pms", $pms);
//$smarty->display("results.tpl");
}
?>
