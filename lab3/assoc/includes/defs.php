<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "pms.php";

function search(&$searchKey) { // pass by ref
  global $pms;
    
  if(empty($searchKey)) 
    return NULL;

  $results = array();
  // Fields to be searched
  $sFields = array('name', 'to', 'from', 'state'); 
  
  foreach($pms as $pm) {
    foreach($sFields as $k => $v) {
      if(stripos($pm[$v], $searchKey) !== FALSE) {
        array_push($results, $pm);
        break;
      }
    }
  }
  
  return $results;
}
?>
