{extends file="layout.tpl"}
{block name="pageTitle"}Advertise Job{/block}
{block name="pageContent"}
                    <fieldset class="">
                    <form role="form">
                        <div class="form-group">
                            <label for="jobTitle">Job Title</label>
                            <input type="email" class="form-control" id="jobTitle" placeholder="Job advert title" required>
                        </div>
                        <div class="form-group">
                            <label for="jobCompany">Company</label>
                            <input type="text" class="form-control" id="jobCompany" placeholder="Company Name" required>
                        </div>
                        <div class="form-group">
                            <label for="jobSalary">Salary</label>
                            <input type="text" class="form-control" id="jobSalary" placeholder="Advertised Salary" required>
                        </div>
                        <div class="form-group">
                            <label for="jobClosingDate">Closing Date</label>
                            <input type="date" class="form-control" id="jobClosingDate" placeholder="Enter email" required>
                        </div>
                        
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    </fieldset>
{/block}