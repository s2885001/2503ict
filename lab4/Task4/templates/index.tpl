{extends file="layout.tpl"}
{block name="pageTitle"}Home{/block}
{block name="pageContent"}
                    <div class="home-img">
                    <img src="img/home.jpg" alt="homepage">
                    </div>
                 
                      <h2>Todays job listings!</h2>
                   
                    
                    <div class="col-sm-6 col-md-3">
                        <div>
                            <div class="li-heading">Full Time</div>
                                <ul>
                                    <li class="li-items">Awesome Job</li>
                                    <li class="li-items">Awesome Job</li>
                                    <li class="li-items">Awesome Job</li>
                                    <li class="li-items">Awesome Job</li>
                                </ul>
                            </div>
                        </div>
                    <div class="col-sm-6 col-md-3">
                        <div>
                            <div class="li-heading">Part Time</div>
                                <ul>
                                    <li class="li-items">Awesome Job</li>
                                    <li class="li-items">Awesome Job</li>
                                    <li class="li-items">Awesome Job</li>
                                    <li class="li-items">Awesome Job</li>
                                </ul>
                            </div>
                        </div>
                    <div class="col-sm-6 col-md-3">
                        <div>
                            <div class="li-heading">Casual</div>
                                <ul>
                                    <li class="li-items">Awesome Job</li>
                                    <li class="li-items">Awesome Job</li>
                                    <li class="li-items">Awesome Job</li>
                                    <li class="li-items">Awesome Job</li>
                                </ul>
                            </div>
                        </div>

{/block}