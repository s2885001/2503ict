<?php
date_default_timezone_set('UTC'); 
/* Functions for user database example. */

/* Load sample data, an array of associative arrays. */
include "users.php";

/* Search sample data for form data $query in name, year or state. */
function search($query) {
    global $users; 

    // Filter $users by $query
    if (!empty($query)) {
	$results = array();
	foreach ($users as $user) {
	    if (stripos($user['name'], $query) !== FALSE ||
	        strpos($user['address'], $query) !== FALSE || 
	        strpos($user['phone'], $query) !== FALSE ||
	        stripos($user['email'], $query) !== FALSE) {
		$results[] = $user;
	    }
	}
	return $results;
    } else {
	return $users;
    }
}
?>
