<?php
/* Library Users.*/
$users = array( 
 array('name' => 'Julia Gillard', 'address' => 'Loganlea Rd, Logan', 
 'phone' => '1234 4321', 'email' => 'julia@the.lodge'), 
 array('name' => 'Tony Abbott', 'address' => 'Logan Rd, Mt Gravatt', 
 'phone' => '9876 5432', 'email' => 'tony@the.hermitage'), 
 array('name' => 'Christine Milne', 'address' => 'Kessels Rd, Nathan', 
 'phone' => '91234 5678', 'email' => 'chris@the.treetop'), 
 array('name' => 'John Mcnabb', 'address' => 'Short Rd, Southport', 
 'phone' => '31464 6473', 'email' => 'john@the.floor'), 
 array('name' => 'Sam Williams', 'address' => 'Long Rd, Ashmore', 
 'phone' => '91234 5678', 'email' => 'sam@the.roof'), 
);
?>
