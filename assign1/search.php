<?PHP /*
/*

Used to search database for specific attributes

*/

date_default_timezone_set("Australia/Brisbane");
include 'includes/Smarty/libs/Smarty.class.php';
require_once('db.php');
  
// Sanitizes variables and trim whitespace from string.
foreach($_POST as $k => $v) {
  $_POST[$k] = trim(sanitize($v) );
}

$smarty = new Smarty;
$dbh = db_open();

// check to see form was submitted perform a search
if(isset($_POST['searchSubmit'])) { 
  $jobResults = searchJobs($_POST['searchIndustry'], $_POST['searchSalary'], $_POST['searchTerm']);
}

// populates industry info used for drop box
try { 
  $query = $dbh->prepare("SELECT industries.id, industries.name FROM industries ORDER BY industries.name ASC"); // ORDER by id
  $query->execute();
  $jobIndustry = $query->fetchAll();
  
} catch(PDOException $e) {
  pdo_error($e);
}
$smarty->assign('searchResults', $jobResults);
$smarty->assign('industries', $jobIndustry);
$smarty->display('search.tpl');

unset($dbh); // closes db
?>
