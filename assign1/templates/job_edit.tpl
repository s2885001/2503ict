{extends file="layout.tpl"}
{block name="pageTitle"}Advertise Job{/block}
{block name="pageContent"}

  {if isset($error)}
    <div class="alert alert-danger">
      <h4>{$error}</h4>
    </div>
  {/if}

  {* Bootstrap delete confirm popup *}
  <div class="modal fade" id="jobDelete" tabindex="-1" role="dialog" aria-labelledby="jobDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="jobDeleteModalLabel">Are you sure you want to delete?</h4>
        </div>
        
        <div class="modal-body">
          <p class="warning text-danger">Are you sure you want to delete this job listing permanently?</p>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger pull-left" href="job_edit.php?id={$smarty.get.id}&amp;delete=true">Delete</a>
        </div>
      </div>
    </div>
  </div>

  <h2 class="pageHeader">Edit Job Details</h2>
  <fieldset class="">
    <form role="form" action="" method="post">
      <div class="form-group">
        <label for="jobTitle">Job Title</label>
        <input type="text" class="form-control" id="jobTitle" name="jobTitle" placeholder="Job advert title" required value="{$job.title}">
      </div>

      <label for="jobSalary">Salary</label>
      <div class="form-group input-group">
        <input type="number" min="0" class="form-control" id="jobSalary" name="jobSalary" placeholder="Advertised Salary" required value="{$job.salary}">
        <span class="input-group-addon">per yr</span>
      </div>

      <div class="form-group">
        <label for="jobLocation">Location</label>
        <input type="text" class="form-control" id="jobLocation" name="jobLocation" placeholder="Job Location" required value="{$job.location}">
      </div>

      <label for="jobDescription">Job Description</label>
      <div class="form-group">
        <textarea class="form-control" id="jobDescription" name="jobDescription" rows="6" maxlength="1500" placeholder="Description of job advertised:" required>{$job.description}</textarea>
      </div>

      <button type="submit" class="btn btn-default btn-success" name="jobSubmit">Save</button>
      {if isset($error)}
        <a class="btn btn-default btn-warning" href="employer_jobs.php?employer={$job.employerID}">Cancel</a>
      {else}
        <button type="button" class="btn btn-default btn-warning" onclick="history.go(-1);">Cancel</button>
      {/if}
      <button type="button" class="btn btn-default btn-danger pull-right" data-toggle="modal" data-target="#jobDelete">Delete</button>
      
    </form>
  </fieldset>
{/block}