{extends file="layout.tpl"}
{block name="pageTitle"}Employer Home{/block}
{block name="pageContent"}

  <h2 class="pageHeader">Employers Job Listings</h2>
  <p>This is the home for all our employer partners.</p>
  <p>Select your company name below from the list to continue.</p>
  <br>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Employer</th><th>Industry</th><th>Description</th>
      </tr>
    </thead>
    <tbody>
      {foreach $employers as $employer}
        <tr>
          <td><a href="employer_job_view.php?employer={$employer.id}"><span class="icon-arrow hidden-xs"></span>{$employer.name}</a></td>
          <td>{$employer.industryName}</td>
          <td>{$employer.description}</td>
        </tr>
      {/foreach}
    </tbody>
  </table>
{/block}