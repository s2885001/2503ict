{extends file="layout.tpl"}
{block name="pageTitle"}View Job{/block}
{block name="pageContent"}


    <h2 class="pageHeader">Job Details</h2>
    <fieldset class="">
      <form role="form">

        <div class="form-group">
          <label for="jobTitle">Title</label>
          <p class="form-control-static" id="jobTitle" name="jobTitle">{$job.title}</p>
        </div>                   

        <label for="jobSalary">Salary</label>
        <div class="form-group input-group">
          <p class="form-control-static" id="jobSalary" name="jobSalary">${$job.salary|number_format}</p>
          <span class="input-group-addon">per yr</span>
        </div>
        
        <div class="form-group">
          <label for="jobIndustry">Industry</label>
          <p class="form-control-static" id="jobIndustry" name="jobIndustry">{$job.industry}</p>
        </div>

        <div class="form-group">
          <label for="jobEmployer">Employer</label>
          <p class="form-control-static" id="jobEmployer" name="jobTitle">{$job.employer}</p>
        </div>
        
        <div class="form-group">
          <label for="jobLocation">Location</label>
          <p class="form-control-static" id="jobLocation" name="jobLocation">{$job.location}</p>
        </div>

        <label for="jobDescription">Job Description</label>
        <div class="form-group">
          <p class="form-control-static" id="jobDescription" name="jobDescription">{$job.description|nl2br}</p>
        </div>
        
      </form>
    </fieldset>


{/block}