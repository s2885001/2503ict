{extends file="layout.tpl"}
{block name="pageTitle"}Jobs Listing{/block}
{block name="pageContent"}

  <h2 class="pageHeader">All Jobs</h2>
  <span>Please select a job entry below from our current employers to view more info.</span>
  <br><br><br>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Title</th><th>Salary</th><th>Location</th><th>Industry</th><th>Employer</th>
      </tr>
    </thead>
    <tbody>
      {foreach $allJobs as $job}
        <tr>
          <td><a href="job_view.php?id={$job.id}"><span class="icon-arrow hidden-xs"></span>{$job.title}</a></td>
          <td>${$job.salary}</td>
          <td>{$job.location}</td>
          <td>{$job.industryName}</td>
          <td>{$job.employer}</td>
        </tr>
        {/foreach}
    </tbody>
  </table>
{/block}