{extends file="layout.tpl"}
{block name="pageTitle"}Employers Job Listing{/block}
{block name="pageContent"}


    <h2 class="pageHeader">{$employerJobs.0.employer}'s Job Listings</h2>
    <span class="hidden-xs">Please click a job listing below to edit or select advertise to create a new listing.</span>
    <a class="popupBtn pull-right" href="advertise.php?employer={$smarty.get.employer}"><span class="icon-resume hidden-xs"></span>Advertise Job</a>
    <br>
    <br>
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Title</th><th>Salary</th><th>Location</th><th>Industry</th>
        </tr>
      </thead>
      <tbody>
        {foreach $employerJobs as $job}
          <tr>
            <td><a href="job_edit.php?id={$job.id}"><span class="icon-arrow hidden-xs"></span>{$job.title}</a></td>
            <td>${$job.salary}</td>
            <td>{$job.location}</td>
            <td>{$job.industryName}</td>
          </tr>
        {/foreach}
      </tbody>
    </table>


{/block}