<?PHP
/*
employers view of jobs
*/
date_default_timezone_set("Australia/Brisbane");
include 'includes/Smarty/libs/Smarty.class.php';
require_once('db.php');

// Sanitizes variables and trim whitespace from string.
foreach($_POST as $k => $v) {
  $_POST[$k] = trim(sanitize($v) );
}

$smarty = new Smarty;
$dbh = db_open();

// retrieves an employers jobs
try { 
  $query = $dbh->prepare("SELECT jobs.id, jobs.title, jobs.salary, jobs.location, industries.name AS industryName, 
                        employers.name AS employer FROM employers, industries, jobs WHERE 
                        employers.id = :employerID AND 
                        jobs.employerID=employers.id AND 
                        employers.industryID=industries.id");
  $query->bindValue(':employerID', (int)$_GET['employer']);
  $query->execute();
  $employerJobs = $query->fetchAll();
  
} catch(PDOException $e) {
  pdo_error($e);
}

$smarty->assign('employerJobs', $employerJobs);
$smarty->display('employer_job_view.tpl');

unset($dbh); // close database
?>