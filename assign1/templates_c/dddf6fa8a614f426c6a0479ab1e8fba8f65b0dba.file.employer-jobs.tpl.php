<?php /* Smarty version Smarty-3.1.16, created on 2014-04-29 14:32:34
         compiled from "./templates/employer-jobs.tpl" */ ?>
<?php /*%%SmartyHeaderCode:80026467535e74fc56bc64-80200732%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dddf6fa8a614f426c6a0479ab1e8fba8f65b0dba' => 
    array (
      0 => './templates/employer-jobs.tpl',
      1 => 1398745952,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1398745304,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '80026467535e74fc56bc64-80200732',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_535e74fc6fc104_00635932',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535e74fc6fc104_00635932')) {function content_535e74fc6fc104_00635932($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Job Listing for Employer</title>
   
    <link rel="stylesheet" href="css/style.css">
    <!-- Bootstrap -->
   <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->    
   <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
  </head>
<body>
      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Job Finder</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
<!--            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li> -->
          </ul>
            <ul class="nav navbar-nav navbar-right">
            <li><a href="employers.php">Employers Home</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="index.php">Home</a>
            <a class="list-group-item" href="search.php">Search for a job</a>
            <a class="list-group-item" href="job_list.php">List all jobs</a>
          </div>
        </div>
        <div class="col-sm-9">
          


    <h2 class="pageHeader">Job listings for <?php echo $_smarty_tpl->tpl_vars['employerJobs']->value[0]['employer'];?>
</h2> 
    <span class="hidden-xs">You can modify or delete a job listing by clicking the title.</span>
    <a class="popupBtn pull-right" href="advertise.php?employer=<?php echo $_GET['employer'];?>
"><span class="icon-resume hidden-xs"></span>Advertise Job</a>

    <table class="table table-hover">
      <thead>
        <tr>
          <th>Title</th><th>Salary <small class="hidden-xs">(p/a)</small></th><th>Location</th><th>Industry</th>
        </tr>
      </thead>
      <tbody>
        <?php  $_smarty_tpl->tpl_vars['job'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['job']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['employerJobs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['job']->key => $_smarty_tpl->tpl_vars['job']->value) {
$_smarty_tpl->tpl_vars['job']->_loop = true;
?>
          <tr>
            <td><a href="job_edit.php?id=<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
"><span class="icon-arrow hidden-xs"></span><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
</a></td>
            <td>$<?php echo $_smarty_tpl->tpl_vars['job']->value['salary'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['job']->value['industryName'];?>
</td>
          </tr>
        <?php } ?>
      </tbody>
    </table>



        </div>
      </div>
    </div>
  </body>
</html><?php }} ?>
