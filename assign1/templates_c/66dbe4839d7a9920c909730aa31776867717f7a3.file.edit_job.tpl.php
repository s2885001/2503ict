<?php /* Smarty version Smarty-3.1.16, created on 2014-04-29 13:55:14
         compiled from "./templates/edit_job.tpl" */ ?>
<?php /*%%SmartyHeaderCode:887828022535efba9bada18-46465403%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '66dbe4839d7a9920c909730aa31776867717f7a3' => 
    array (
      0 => './templates/edit_job.tpl',
      1 => 1398733621,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1398742719,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '887828022535efba9bada18-46465403',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_535efba9d91094_84129011',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535efba9d91094_84129011')) {function content_535efba9d91094_84129011($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Advertise Job</title>
   
    <link rel="stylesheet" href="css/style.css">
    <!-- Bootstrap -->
   <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->    
   <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
  </head>
<body>
      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Job Finder</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
<!--            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li> -->
          </ul>
            <ul class="nav navbar-nav navbar-right">
            <li><a href="employers.php">Employers Home</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="index.php">Home</a>
            <a class="list-group-item" href="search.php">Search for a job</a>
            <a class="list-group-item" href="all-jobs.php">List all jobs</a>
          </div>
        </div>
        <div class="col-sm-9">
          

  <?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?>
    <div class="alert alert-danger">
      <h4><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</h4>
    </div>
  <?php }?>

  <h2 class="pageHeader">Edit Job Details</h2>
  <fieldset class="">
    <form role="form" action="" method="post">
      <div class="form-group">
        <label for="jobTitle">Job Title</label>
        <input type="text" class="form-control" id="jobTitle" name="jobTitle" placeholder="Job advert title" required value="<?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
">
      </div>

      <label for="jobSalary">Salary</label>
      <div class="form-group input-group">
        <input type="number" min="0" class="form-control" id="jobSalary" name="jobSalary" placeholder="Advertised Salary" required value="<?php echo $_smarty_tpl->tpl_vars['job']->value['salary'];?>
">
        <span class="input-group-addon">per annum</span>
      </div>

      <div class="form-group">
        <label for="jobLocation">Location</label>
        <input type="text" class="form-control" id="jobLocation" name="jobLocation" placeholder="Job Location" required value="<?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>
">
      </div>

      <div class="form-group"> 
        <label for="jobIndustry">Industry</label>
        <span class="form-caption hidden-xs pull-right">This field is automatically selected from your employer profile.</span>
        <input type="text" class="form-control" id="jobIndustry" name="jobIndustry" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['industry'];?>
" disabled>
      </div>

      <label for="jobDescription">Job Description</label>
      <div class="form-group">
        <textarea class="form-control" id="jobDescription" name="jobDescription" rows="6" maxlength="1500" placeholder="Description of job position on offer..." required><?php echo $_smarty_tpl->tpl_vars['job']->value['description'];?>
</textarea>
      </div>

      <button type="submit" class="btn btn-default btn-success" name="jobSubmit">Save</button>
      <?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?>
        
        <a class="btn btn-default btn-warning" href="employer_jobs.php?employer=<?php echo $_smarty_tpl->tpl_vars['job']->value['employerID'];?>
">Cancel</a>
      <?php } else { ?>
        <button type="button" class="btn btn-default btn-warning" onclick="history.go(-1);">Cancel</button>
      <?php }?>
      <button type="button" class="btn btn-default btn-danger pull-right" data-toggle="modal" data-target="#jobDeleteModal">Delete</button>
    </form>
  </fieldset>

  
  <div class="modal fade" id="jobDeleteModal" tabindex="-1" role="dialog" aria-labelledby="jobDeleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="jobDeleteModalLabel">Delete this job advert?</h4>
        </div>
        
        <div class="modal-body">
          <p>You are about to delete the job advertisement titled; <span class="title text-primary"><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
</span> </p>
          <p>By deleting this job advertisement it will no longer be accessible by anyone, yourself and job seekers included.</p>
          <p class="warning text-danger">Are you sure you want to permanently delete this job listing?</p>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger pull-left" href="edit_job.php?id=<?php echo $_GET['id'];?>
&amp;delete=true">Delete!</a>
        </div>
      </div>
    </div>
  </div>

        </div>
      </div>
    </div>
  </body>
</html><?php }} ?>
