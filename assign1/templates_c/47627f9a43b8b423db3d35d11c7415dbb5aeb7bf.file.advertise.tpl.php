<?php /* Smarty version Smarty-3.1.16, created on 2014-04-30 00:51:08
         compiled from "./templates/advertise.tpl" */ ?>
<?php /*%%SmartyHeaderCode:73377132533d398e42e092-72492310%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '47627f9a43b8b423db3d35d11c7415dbb5aeb7bf' => 
    array (
      0 => './templates/advertise.tpl',
      1 => 1398781632,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1398777505,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '73377132533d398e42e092-72492310',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_533d398e49fca5_98351799',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_533d398e49fca5_98351799')) {function content_533d398e49fca5_98351799($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Advertise a Job</title>
   
    <link rel="stylesheet" href="css/style.css">
    <!-- Bootstrap -->
   <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->    
   <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
  </head>
<body>
      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Job Finder</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
          </ul>
            <ul class="nav navbar-nav navbar-right">
            <li><a href="employers.php">Employers Home</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="index.php">Home</a>
            <a class="list-group-item" href="search.php">Search for a job</a>
            <a class="list-group-item" href="job_list.php">List all jobs</a>
          </div>
        </div>
        <div class="col-sm-9">
          
                    <fieldset class="">
                    <form role="form">
                        <div class="form-group">
                            <label for="jobTitle">Job Title</label>
                            <input type="text" class="form-control" id="jobTitle" placeholder="Job Title" required>
                        </div>
                        <div class="form-group">
                            <label for="jobCompany">Company</label>
                            <input type="text" class="form-control" id="jobCompany" placeholder="Company Name" required>
                        </div>
                        <div class="form-group">
                            <label for="jobSalary">Salary</label>
                            <input type="text" class="form-control" id="jobSalary" placeholder="Salary" required>
                        </div>
                        <div class="form-group">
                            <label for="jobClosingDate">Closing Date</label>
                            <input type="date" class="form-control" id="jobClosingDate" placeholder="Email" required>
                        </div>
                        
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    </fieldset>

        </div>
      </div>
    </div>
  </body>
  <div id="footer">
  <div class="container">
      	<div class="row text-center">
          <br><br><br>
          <p><b>This project has been completed by Daniel Cousins s2885001.</b></p>
           <a href="docs/doc.html" title="Documentation">Click here to view documentation for application.</a>
		</div>
  </div>
</div>
</html><?php }} ?>
