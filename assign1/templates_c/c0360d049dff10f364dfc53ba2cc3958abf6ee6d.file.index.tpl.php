<?php /* Smarty version Smarty-3.1.16, created on 2014-04-29 23:18:46
         compiled from "./templates/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1634140956533d382f004288-46831486%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c0360d049dff10f364dfc53ba2cc3958abf6ee6d' => 
    array (
      0 => './templates/index.tpl',
      1 => 1398743631,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1398777505,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1634140956533d382f004288-46831486',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_533d382f092f86_12656219',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_533d382f092f86_12656219')) {function content_533d382f092f86_12656219($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
   
    <link rel="stylesheet" href="css/style.css">
    <!-- Bootstrap -->
   <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->    
   <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
  </head>
<body>
      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Job Finder</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
          </ul>
            <ul class="nav navbar-nav navbar-right">
            <li><a href="employers.php">Employers Home</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="index.php">Home</a>
            <a class="list-group-item" href="search.php">Search for a job</a>
            <a class="list-group-item" href="job_list.php">List all jobs</a>
          </div>
        </div>
        <div class="col-sm-9">
          
                    <div class="home-img">
                    <img src="image/home.jpg" alt="homepage">
                    </div>
                 
                      <h2>Todays job listings!</h2>
                   
                    
                    <div class="col-sm-6 col-md-3">
                        <div>
                            <div class="li-heading">Full Time</div>
                                <ul>
                                    <li class="li-items">JAVA Programmer</li>
                                    <li class="li-items">Clothing Expert</li>
                                    <li class="li-items">Pizza Maker</li>
                                    <li class="li-items">Admin Assistant</li>
                                </ul>
                            </div>
                        </div>
                    <div class="col-sm-6 col-md-3">
                        <div>
                            <div class="li-heading">Part Time</div>
                                <ul>
                                    <li class="li-items">Pizza Maker</li>
                                    <li class="li-items">Admin Assistant</li>
                                    <li class="li-items">JAVA Programmer</li>
                                    <li class="li-items">Clothing Expert</li>
                                </ul>
                            </div>
                        </div>
                    <div class="col-sm-6 col-md-3">
                        <div>
                            <div class="li-heading">Casual</div>
                                <ul>
                                    <li class="li-items">Carpenter</li>
                                    <li class="li-items">Sport Physician</li>
                                    <li class="li-items">Concreter</li>
                                    <li class="li-items">Chef</li>
                                </ul>
                            </div>
                        </div>


        </div>
      </div>
    </div>
  </body>
  <div id="footer">
  <div class="container">
      	<div class="row text-center">
          <br><br><br>
          <p><b>This project has been completed by Daniel Cousins s2885001.</b></p>
           <a href="docs/doc.html" title="Documentation">Click here to view documentation for application.</a>
		</div>
  </div>
</div>
</html><?php }} ?>
