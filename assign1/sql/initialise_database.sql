DROP TABLE IF EXISTS employers;
DROP TABLE IF EXISTS industries;
DROP TABLE IF EXISTS jobs;


CREATE TABLE IF NOT EXISTS employers (
	id INTEGER PRIMARY KEY,
	name VARCHAR(32) DEFAULT '' NOT NULL UNIQUE COLLATE NOCASE, /* 'COLLATE NOCASE' = case insensitive */
	industryID INTEGER DEFAULT '0' NOT NULL,
	description TEXT);
	
CREATE TABLE IF NOT EXISTS industryOptions (
	id INTEGER PRIMARY KEY,
	name VARCHAR(24) DEFAULT '' NOT NULL UNIQUE COLLATE NOCASE); /* 'COLLATE NOCASE' = case insensitive */
	
CREATE TABLE IF NOT EXISTS jobs (
	id INTEGER PRIMARY KEY,
	employerID INTEGER DEFAULT '0' NOT NULL,
	title VARCHAR(32),
	description TEXT,
	location VARCHAR(32),
	salary INTEGER);
	
/* ------------------------------------------------------------------------------------- */

INSERT INTO industries(id, name)VALUES (1, "Construction");
INSERT INTO industries(id, name)VALUES (2, "Administration");
INSERT INTO industries(id, name)VALUES (3, "Health & Medicine");
INSERT INTO industries(id, name)VALUES (4, "Technology");
INSERT INTO industries(id, name)VALUES (5, "Retail");
INSERT INTO industries(id, name)VALUES (6, "Hospitality & Tourism");


INSERT INTO employers(id, name, industryID, description)
	VALUES 	(1, "Bob the Builder", 1, "We are making the big buildings in town!");	
INSERT INTO employers(id, name, industryID, description)
	VALUES 	(2, "Samuel Legal", 2, "The best place for your legal needs.");
INSERT INTO employers(id, name, industryID, description)
	VALUES 	(3, "Smart Clothing", 5, "Best prices on the most upto date fashion!");
INSERT INTO employers(id, name, industryID, description)
	VALUES 	(4, "Pizza Hut", 6, "Pizza is the best and we make the best pizza.");
INSERT INTO employers(id, name, industryID, description)
	VALUES 	(5, "Biotech Sports", 3, "We are leading the field in sports medicine.");
INSERT INTO employers(id, name, industryID, description)
	VALUES 	(6, "InfoDesign", 4, "Software design company.");


INSERT INTO jobs(employerID, location, salary, title, description)
	VALUES 	(1, "Broadbeach", 46000, "Formwork Carpenters / Concreter", "Our company is seeking a skilled formwork carpenter and concreter in the broadbeach region.");
INSERT INTO jobs(employerID, location, salary, title, description)
	VALUES 	(2, "Labrador", 35000, "Administrative Assistant", "Looking for someone to make part of our team to perform several administative tasks.");
INSERT INTO jobs(employerID, location, salary, title, description)
	VALUES 	(5, "Logan", 90000, "Sport Physician", "An exciting opportunity exists for a specialist in the field of sports medicine.");
INSERT INTO jobs(employerID, location, salary, title, description)
	VALUES 	(3, "Southport", 45000, "Clothing Expert", "Are you looking for an exciting opportunity in an industry leading fashion outlet? Look no further!");
INSERT INTO jobs(employerID, location, salary, title, description)
	VALUES 	(6, "Burleigh Heads", 87000, "JAVA Programmer", "JAVA Software engineer is required to assist our team complete their current project.");
