<?PHP
/*
returns all jobs in employer view
*/
date_default_timezone_set("Australia/Brisbane");
include 'includes/Smarty/libs/Smarty.class.php';
require_once('db.php');

// Sanitizes variables and trim whitespace from string.
foreach($_POST as $k => $v) {
  $_POST[$k] = trim(sanitize($v) );
}

$smarty = new Smarty;
$dbh = db_open();

try {
  $query = $dbh->prepare("SELECT jobs.id, jobs.title, jobs.salary, jobs.location, industries.name AS industryName, employers.name AS employer 
                        FROM jobs, industries, employers WHERE 
                        jobs.employerID=employers.id AND 
                        employers.industryID=industries.id");
  $query->execute();
  $allJobs = $query->fetchAll();
  
} catch(PDOException $e) {
  pdo_error($e);
}

$smarty->assign('allJobs', $allJobs);
$smarty->display('jobs_all.tpl');

unset($dbh); // closed db
?>