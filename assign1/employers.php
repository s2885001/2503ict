<?PHP
/*
  Used to display current employers
*/
date_default_timezone_set("Australia/Brisbane");
include 'includes/Smarty/libs/Smarty.class.php';
require_once('db.php');

// Sanitizes variables and trim whitespace from string.
foreach($_POST as $k => $v) {
  $_POST[$k] = trim(sanitize($v) );
}

$smarty = new Smarty;
$dbh = db_open();

// retrieves list of employers
try { 
  $query = $dbh->prepare("SELECT employers.id, employers.name, employers.description, industries.name AS industryName FROM employers, industries WHERE 
                        employers.industryID=industries.id
                        ORDER BY employers.id ASC"); // ORDER by id
  $query->execute();
  $allEmployers = $query->fetchAll();
  
} catch(PDOException $e) {
  pdo_error($e);
}

$smarty->assign('employers', $allEmployers);
$smarty->display('index_employers.tpl');

unset($dbh); // close database
?>