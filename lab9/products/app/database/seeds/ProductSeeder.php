<?php 

class ProductSeeder extends Seeder {

	public function run()
	{
		$product = new Product;
		$product->name = 'Red Soda';
		$product->price = 1.99;
		$product->save();

		$product = new Product;
		$product->name = 'Green Soda';
		$product->price = 1.99;
		$product->save();

	}
}